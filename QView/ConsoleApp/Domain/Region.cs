﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Region
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(length: 50), MinLength(length: 3)]
        public string Name { get; set; }

        [MaxLength(length: 350)]
        public string Description { get; set; }

        public virtual List<City> Cities { get; set; } = new List<City>();

        public Region(string name)
        {
            this.Name = name;
        }

        public Region(string name, string description)
        {
            this.Name = name;
            this.Description = description;
        }

        public Region()
        {
            
        }
    }
}
