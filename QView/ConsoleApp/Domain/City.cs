﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class City
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(length: 50), MinLength(length: 3)]
        public string Name { get; set; }

        [Required]
        public int RegionId { get; set; }

        [ForeignKey(name: "RegionId")]
        public virtual Region Region { get; set; }

        public virtual List<Office> Offices { get; set; } = new List<Office>();

        public City (string name, int regionId)
        {
            this.Name = name;
            this.RegionId = regionId;
        }

        public City()
        {
            
        }
    }
}
