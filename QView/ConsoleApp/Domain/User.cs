﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class User
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(length: 50), MinLength(length: 3)]
        public string UserName { get; set; }

        [Required]
        [MaxLength(length: 50), MinLength(length: 1)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(length: 50), MinLength(length: 1)]
        public string LastName { get; set; }

        [Required]
        [MaxLength(length: 512), MinLength(length: 8)]
        public string Password { get; set; }

        [Required]
        [MaxLength(length: 30), MinLength(length: 3)]
        public string MobileNumber { get; set; }

        [Required]
        [MaxLength(length: 30), MinLength(length: 5)]
        public string Email { get; set; }

        [Required]
        public DateTime DateCreated { get; set; } 

        public DateTime? DateClosed { get; set; }
        [Required]
        public int UserTypeId { get; set; }

        [ForeignKey(name: "UserTypeId")]
        public virtual UserType UserType { get; set; }

        public virtual List<UserFavourite> UserFavourites { get; set; } = new List<UserFavourite>();

        #region NotMapped
        public string FirstLastName => $"{FirstName} {LastName}";

        public string LastFirstName => $"{LastName} {FirstName}";

        #endregion
        
        public User(string userName, string firstName, string lastName, string password, string mobileNumber, string email, int userType)
        {
            this.UserName = userName;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Password = password;
            this.Email = email;
            this.MobileNumber = mobileNumber;
            this.UserTypeId = userType;
            this.DateCreated = DateTime.Now;
        }

        public User()
        {
            this.DateCreated = DateTime.Now;
        }

        public override string ToString()
        {
            StringBuilder b = new StringBuilder();

            b.Append(value: $"Id: {Id}\n");
            b.Append(value: $"FirstLastName: {FirstLastName}\n");
            b.Append(value: $"PassWord: {Password}\n");
            b.Append(value: $"Email: {Email}\n");
            b.Append(value: $"Mobile: {MobileNumber}\n");
            b.Append(value: $"DateCreated: {DateCreated}\n");
            b.Append(value: $"UserTypeId: {UserTypeId}\n");
            b.Append(value: $"UserType: {UserType}\n");

            return b.ToString();
        }
    }
}
