﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class UserPermission
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(length: 50)]
        public string Permission { get; set; }

        [MaxLength(length: 350)]
        public string Description { get; set; }

        public virtual List<PermissionApplied> PermissionApplieds { get; set; } = new List<PermissionApplied>();

        public UserPermission(string permission)
        {
            this.Permission = permission;
        }
    }
}
