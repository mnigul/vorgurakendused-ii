﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Office
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(length: 50), MinLength(length: 3)]
        public string Name { get; set; }

        [Required]
        [MaxLength(length: 50), MinLength(length: 3)]
        public string Address { get; set; }

        [MaxLength(length: 50)]
        public string OpeningHours { get; set; }

        [Required]
        public int CityId { get; set; }

        [ForeignKey(name: "CityId")]
        public virtual City City { get; set; }

        public int CompanyId { get; set; }

        [ForeignKey(name: "CompanyId")]
        public virtual Company Company { get; set; }

        public virtual List<UserFavourite> UserFavourites { get; set; } = new List<UserFavourite>();

        public virtual List<TakenNumber> TakenNumbers { get; set; } = new List<TakenNumber>();

        public Office(string name, string aadress, string openingHours, int cityId, int companyId)
        {
            this.Name = name;
            this.Address = aadress;
            this.OpeningHours = openingHours;
            this.CityId = cityId;
            this.CompanyId = companyId;
        }

        public Office()
        {
            
        }
    }
}
