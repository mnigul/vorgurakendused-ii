﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Enums;

namespace Domain
{
    public class TakenNumber
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public int Number { get; set; }

        public DateTime DateTaken { get; set; }

        [Required]
        public int OfficeId { get; set; }

        [ForeignKey(name: "OfficeId")]
        public virtual Office Office { get; set; }

        public TakenNumType TakenNumType { get; set; }

        public DateTime? DateService { get; set; }

        public int? WaitTime { get; set; }

        public TakenNumber(int number, int officeId, TakenNumType takenNumType)
        {
            this.Number = number;
            this.OfficeId = officeId;
            this.TakenNumType = takenNumType;
            DateTaken = DateTime.Now;
        }

        public TakenNumber()
        {
            
        }

        public override string ToString()
        {
            StringBuilder b = new StringBuilder();

            b.Append(value: $"Id: {Id}");
            b.Append(value: $"Number: {Number}");
            b.Append(value: $"DateTaken: {DateTaken}");
            b.Append(value: $"OfficeId: {OfficeId}");
            b.Append(value: $"DateService: {DateService}");
            b.Append(value: $"WaitTime: {WaitTime}");

            return b.ToString();
        }
    }
}
