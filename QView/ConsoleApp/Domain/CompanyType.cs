﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class CompanyType
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(length: 50)]
        public string Name { get; set; }

        [MaxLength(length: 350)]
        public string Description { get; set; }

        public virtual List<Company> Companies { get; set; } = new List<Company>();

        public CompanyType(string name)
        {
            this.Name = name;
        }

        public CompanyType(string name, string description)
        {
            this.Name = name;
            this.Description = description;
        }

        public CompanyType()
        {
        }
    }
}
