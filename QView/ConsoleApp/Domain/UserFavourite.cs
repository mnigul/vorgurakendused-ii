﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class UserFavourite
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public DateTime DateCreated { get; set; }

        public DateTime? DateEnded { get; set; }

        [Required]
        public int UserId { get; set; }

        [ForeignKey(name: "UserId")]
        public virtual User User { get; set; }

        [Required]
        public int OfficeId { get; set; }

        [ForeignKey(name: "OfficeId")]
        public virtual Office Office { get; set; }

        public UserFavourite(int userId, int officeId)
        {
            this.UserId = userId;
            this.OfficeId = officeId;
            this.DateCreated = DateTime.Now;
        }

        public UserFavourite()
        {
           
        }

    }
}
