﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class UserType
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(length: 50)]
        public string Name { get; set; }

        [MaxLength(length: 350)]
        public string Description { get; set; }

        public virtual List<PermissionApplied> PermissionApplieds { get; set; } = new List<PermissionApplied>();

        public virtual List<User> Users { get; set; } = new List<User>();

        public UserType()
        {
            
        }

        public UserType(string name)
        {
            this.Name = name;
        }

        public UserType(string name, string description)
        {
            this.Name = name;
            this.Description = description;
        }
    }
}
