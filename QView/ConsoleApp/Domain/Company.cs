﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Company
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(length: 50)]
        public string Name { get; set; }
        [Required]
        public int CompanyTypeId { get; set; }

        [ForeignKey(name: "CompanyTypeId")]
        public virtual CompanyType CompanyType { get; set; }

        public virtual List<Office> Offices { get; set; } = new List<Office>();

        public Company(string name, int companyTypeId)
        {
            this.Name = name;
            this.CompanyTypeId = companyTypeId;
        }

        public Company(string name)
        {
            this.Name = name;
        }

        public Company()
        {

        }
    }
}
