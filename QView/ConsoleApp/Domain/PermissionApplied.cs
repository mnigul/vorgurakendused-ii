﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class PermissionApplied
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public DateTime DateCreated { get; set; }

        public DateTime? DateClosed { get; set; }

        [Required]
        public int UserTypeId { get; set; }

        [ForeignKey(name: "UserTypeId")]
        public virtual UserType UserType { get; set; }

        [Required]
        public int UserPermissionId { get; set; }

        [ForeignKey(name: "UserPermissionId")]
        public virtual UserPermission UserPermission { get; set; }

        public PermissionApplied(int userTypeId, int userPermissionId)
        {
            this.UserPermissionId = userPermissionId;
            this.UserTypeId = userTypeId;
            this.DateCreated = DateTime.Now;
        }
    }
}
