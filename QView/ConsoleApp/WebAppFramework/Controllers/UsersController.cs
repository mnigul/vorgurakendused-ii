﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DAL;
using DAL.Repositories;
using Domain;
using Interfaces;

namespace WebAppFramework.Controllers
{
    public class UsersController : Controller
    {
        private static readonly AppDbContext _appDbContext = new AppDbContext();
        private readonly IUserRepository _userRepository = new UserRepository(dbContext: _appDbContext);
        // GET: Users
        public ActionResult Index()
        {
            return View(model: _userRepository.All);
        }

        // GET: Users/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(statusCode: HttpStatusCode.BadRequest);
            }
            User user = _userRepository.Find(id: id.Value);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(model: user);
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            IUserTypeRepository userTypeRepository = new UserTypeRepository(dbContext: _appDbContext);
            ViewBag.UserTypeId = new SelectList(items: userTypeRepository.All, dataValueField: "Id", dataTextField: "Name");
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,UserName,FirstName,LastName,Password,MobileNumber,Email,UserTypeId")] User user)
        {
            user.DateCreated = DateTime.Now;
            if (ModelState.IsValid)
            {
                _userRepository.Add(entity: user);
                _userRepository.SaveChanges();
                return RedirectToAction(actionName: "Index");
            }
            IUserTypeRepository userTypeRepository = new UserTypeRepository(dbContext: _appDbContext);
            ViewBag.UserTypeId = new SelectList(userTypeRepository.All, "Id", "Name", user.UserTypeId);
            return View(model: user);
        }

        // GET: Users/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = _userRepository.Find(id: id.Value);
            if (user == null)
            {
                return HttpNotFound();
            }
            //ViewBag.UserTypeId = new SelectList(db.UserTypes, "Id", "Name", user.UserTypeId);
            return View(model: user);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,UserName,FirstName,LastName,Password,MobileNumber,Email,DateCreated,DateClosed,UserTypeId")] User user)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(user).State = EntityState.Modified;
                _userRepository.Update(entity: user);
                _userRepository.SaveChanges();
                return RedirectToAction(actionName: "Index");
            }
            //ViewBag.UserTypeId = new SelectList(db.UserTypes, "Id", "Name", user.UserTypeId);
            return View(model: user);
        }

        // GET: Users/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(statusCode: HttpStatusCode.BadRequest);
            }
            User user = _userRepository.Find(id: id.Value);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(model: user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _userRepository.Remove(id: id);
            _userRepository.SaveChanges();
            return RedirectToAction(actionName: "Index");
        }
        /*
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        */
    }
}
