﻿using System.Collections.Generic;
using BL.DTOs;

namespace BL.Interfaces
{
    public interface ICompanyTypeService : IService
    {
        CompanyTypeDTO GetById(int id);

        List<CompanyTypeDTO> GetAll();

        CompanyTypeDTO AddNewCompanyType(CompanyTypeDTO companyTypeDto);
    }
}
