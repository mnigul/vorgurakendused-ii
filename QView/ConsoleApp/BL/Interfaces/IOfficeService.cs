﻿using System.Collections.Generic;
using BL.DTOs;

namespace BL.Interfaces
{
    public interface IOfficeService : IService
    {
        OfficeDTO GetById(int id);
        List<OfficeDTO> GetAll();
        OfficeDTO AddNewOffice(OfficeDTO officeDto);
    }
}
