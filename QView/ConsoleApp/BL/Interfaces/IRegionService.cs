﻿using System.Collections.Generic;
using BL.DTOs;

namespace BL.Interfaces
{
    public interface IRegionService : IService
    {
        RegionDTO GetById(int id);

        List<RegionDTO> GetAll();

        RegionDTO AddNewRegion(RegionDTO regionDto);
    }
}
