﻿using System.Collections.Generic;
using BL.DTOs;

namespace BL.Interfaces
{
    public interface ICompanyService : IService
    {
        CompanyDTO GetById(int id);

        List<CompanyDTO> GetAll();

        CompanyDTO AddNewCompany(CompanyDTO companyDto);
    }
}
