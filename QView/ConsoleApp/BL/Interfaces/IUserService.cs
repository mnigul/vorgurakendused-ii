﻿using System.Collections.Generic;
using BL.DTOs;

namespace BL.Interfaces
{
    public interface IUserService : IService
    {
        UserDTO GetById(int id);

        List<UserDTO> GetAll();

        UserDTO AddNewUser(UserDTO userDto);
    }
}
