﻿using System;

namespace BL.Interfaces
{
    public interface IService
    {
        string LastErrorMessage { get;}
        bool ValidateDomainModel(Object u);

        DateTime ValidateDateTimeForDateTimeNow(DateTime d);
        DateTime ValidateDateTimeForSqlDateTimeMin(DateTime d);
    }
}
