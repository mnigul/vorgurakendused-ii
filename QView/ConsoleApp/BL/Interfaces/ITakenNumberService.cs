﻿using System.Collections.Generic;
using BL.DTOs;

namespace BL.Interfaces
{
    public interface ITakenNumberService : IService
    {
        TakenNumberDTO GetById(int id);
        List<TakenNumberDTO> GetAll();
        TakenNumberDTO AddNewTakenNumber(TakenNumberDTO takenNumberDto);
    }
}
