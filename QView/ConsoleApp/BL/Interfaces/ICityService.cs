﻿using System.Collections.Generic;
using BL.DTOs;

namespace BL.Interfaces
{
    public interface ICityService : IService
    {
        CityDTO GetById(int id);

        List<CityDTO> GetAll();

        CityDTO AddNewCity(CityDTO cityDto);
    }
}
