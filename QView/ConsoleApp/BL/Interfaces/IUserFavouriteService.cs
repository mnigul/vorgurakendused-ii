﻿using System.Collections.Generic;
using BL.DTOs;

namespace BL.Interfaces
{
    public interface IUserFavouriteService : IService
    {
        UserFavouriteDTO GetById(int id);
        List<UserFavouriteDTO> GetAll();
        UserFavouriteDTO AddNewUserFavourite(UserFavouriteDTO userDto);
    }
}
