﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.DTOs
{
    public class UserFavouriteDTO
    {
        public int Id { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateEnded { get; set; }
        public int OfficeId { get; set; }
        public int UserId { get; set; }
    }
}
