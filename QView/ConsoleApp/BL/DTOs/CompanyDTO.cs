﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.DTOs
{
    public class CompanyDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CompanyTypeId { get; set; }
        public virtual CompanyType CompanyType { get; set; }
        public virtual List<Office> Offices { get; set; } = new List<Office>();

    }
}
