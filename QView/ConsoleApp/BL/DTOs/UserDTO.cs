﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.DTOs
{
    public class UserDTO
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PassWord { get; set; }
        public string MobileNumber { get; set; }
        public string Email { get; set; }
        public DateTime DateCreated { get; set; }
        public int UserTypeId { get; set; }
    }
}
