﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using Domain.Enums;

namespace BL.DTOs
{
    public class TakenNumberDTO
    {
        public int Id { get; set; }
        public int Number { get; set; }

        public DateTime DateTaken { get; set; }

        public int OfficeId { get; set; }

        public DateTime? DateService { get; set; }

        public int? WaitTime { get; set; }
    }
}
