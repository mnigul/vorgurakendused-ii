﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using BL.DTOs;
using BL.Interfaces;
using BL.ObjectFactories;
using Domain;
using Interfaces.Repositories;

namespace BL.Services
{
    public class RegionService : Service, IRegionService
    {
        private readonly IRegionRepository _regionRepository;
        private readonly RegionFactory _regionFactory;

        public RegionService(IRegionRepository regionRepository)
        {
           _regionRepository = regionRepository;

            _regionFactory = new RegionFactory();
        }

        /**
         * Get Region as DTO from database by ID
         */
        public RegionDTO GetById(int id)
        {
            var region = _regionRepository.Find(id: id);
            return _regionFactory.Create(region: region);
        }

        /**
         * Get all Region-s as DTOs from database
         */
        public List<RegionDTO> GetAll()
        {
            return _regionRepository
                .All
                .Select(selector: x => _regionFactory.Create(region: x))
                .ToList();
        }

        /**
         *  Add a new Region to database
         */
        public RegionDTO AddNewRegion(RegionDTO regionDto)
        {
            //create Region model
            Region domain = _regionFactory.Create(regionDto: regionDto);
            //add missing region type by id

#if DEBUG
            Trace.Write(message: domain.ToString());
#endif

            if (!ValidateDomainModel(u: domain))
            {
                return null;
            }

            var ret = _regionRepository.Add(entity: domain);
            _regionRepository.SaveChanges();

            return _regionFactory.Create(region: ret);
        }
    }


}
