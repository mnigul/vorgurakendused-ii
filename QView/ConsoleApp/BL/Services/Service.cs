﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL.Interfaces;
using Domain;

namespace BL.Services
{
    public class Service : IService
    {
        public string LastErrorMessage { get; private set; }


        public bool ValidateDomainModel(Object u)
        {
            var context = new ValidationContext(instance: u, serviceProvider: null, items: null);
            var validationResults = new List<ValidationResult>();

            bool isValid = Validator
                .TryValidateObject(
                    instance: u,
                    validationContext: context,
                    validationResults: validationResults,
                    validateAllProperties: true);

            StringBuilder builder = new StringBuilder();
            foreach (var item in validationResults)
            {
                builder.Append(value: item.ErrorMessage);
            }
            LastErrorMessage = builder.ToString();

            return isValid;
        }

        public DateTime ValidateDateTimeForDateTimeNow(DateTime d)
        {
            if (d < (DateTime)SqlDateTime.MinValue)
            {
                return DateTime.Now;
            }
            return d;
        }

        public DateTime ValidateDateTimeForSqlDateTimeMin(DateTime d)
        {
            if (d < (DateTime)SqlDateTime.MinValue)
            {
                return (DateTime) SqlDateTime.MinValue;
            }
            return d;
        }

    }
}
