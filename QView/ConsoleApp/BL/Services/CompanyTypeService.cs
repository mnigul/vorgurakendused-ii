﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using BL.DTOs;
using BL.Interfaces;
using BL.ObjectFactories;
using Domain;
using Interfaces.Repositories;

namespace BL.Services
{
    public class CompanyTypeService : Service, ICompanyTypeService
    {
        private readonly ICompanyTypeRepository _companyTypeRepository;
        private readonly CompanyTypeFactory _companyTypeFactory;
        //private readonly ICompanyTypeRepository _companyTypeRepository;


        public CompanyTypeService(ICompanyTypeRepository companyTypeRepository)
        {
            _companyTypeRepository = companyTypeRepository;

            _companyTypeFactory = new CompanyTypeFactory();
        }

        /**
         * Get Company as DTO from database by ID
         */
        public CompanyTypeDTO GetById(int id)
        {
            var companyType = _companyTypeRepository.Find(id: id);
            return _companyTypeFactory.Create(companyType: companyType);
        }

        /**
         * Get all Company-s as DTOs from database
         */
        public List<CompanyTypeDTO> GetAll()
        {
            return _companyTypeRepository
                .All
                .Select(selector: x => _companyTypeFactory.Create(companyType: x))
                .ToList();
        }

        /**
         *  Add a new Company to database
         */
        public CompanyTypeDTO AddNewCompanyType(CompanyTypeDTO companyTypeDto)
        {
            //create Company model
            CompanyType domain = _companyTypeFactory.Create(companyTypeDto: companyTypeDto);
            //add missing Company type by id

#if DEBUG
            Trace.Write(message: domain.ToString());
#endif

            if (!ValidateDomainModel(u: domain))
            {
                return null;
            }

            var ret = _companyTypeRepository.Add(entity: domain);
            _companyTypeRepository.SaveChanges();

            return _companyTypeFactory.Create(companyType: ret);
        }


    }
}
