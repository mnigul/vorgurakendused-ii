﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using BL.DTOs;
using BL.Interfaces;
using BL.ObjectFactories;
using Domain;
using Interfaces.Repositories;

namespace BL.Services
{
    public class CityService : Service, ICityService
    {
        private readonly ICityRepository _cityRepository;
        private readonly CityFactory _cityFactory;

        public CityService(ICityRepository cityRepository)
        {
            _cityRepository = cityRepository;

            _cityFactory = new CityFactory();
        }

        /**
         * Get City as DTO from database by ID
         */
        public CityDTO GetById(int id)
        {
            var city = _cityRepository.Find(id: id);
            return _cityFactory.Create(city: city);
        }

        /**
         * Get all City-s as DTOs from database
         */
        public List<CityDTO> GetAll()
        {
            return _cityRepository
                .All
                .Select(selector: x => _cityFactory.Create(city: x))
                .ToList();
        }

        /**
         *  Add a new City to database
         */
        public CityDTO AddNewCity(CityDTO cityDto)
        {
            //create City model
            City domain = _cityFactory.Create(cityDto: cityDto);
            //add missing city type by id

#if DEBUG
            Trace.Write(message: domain.ToString());
#endif

            if (!ValidateDomainModel(u: domain))
            {
                return null;
            }

            var ret = _cityRepository.Add(entity: domain);
            _cityRepository.SaveChanges();

            return _cityFactory.Create(city: ret);
        }
    }
}
