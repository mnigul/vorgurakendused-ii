﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using BL.DTOs;
using BL.Interfaces;
using BL.ObjectFactories;
using Domain;
using Interfaces.Repositories;

namespace BL.Services
{
    public class OfficeService : Service, IOfficeService
    {
        private readonly IOfficeRepository _officeRepository;
        private readonly OfficeFactory _officeFactory;

        public OfficeService(IOfficeRepository officeRepository)
        {
            _officeRepository = officeRepository;
            _officeFactory = new OfficeFactory();
        }

        /**
         * Get Office as DTO from database by ID
         */
        public OfficeDTO GetById(int id)
        {
            var office = _officeRepository.Find(id: id);
            return _officeFactory.Create(office: office);
        }

        /**
         * Get all Office-s as DTOs from database
         */
        public List<OfficeDTO> GetAll()
        {
            return _officeRepository
                .All
                .Select(selector: x => _officeFactory.Create(office: x))
                .ToList();
        }

        /**
         *  Add a new Office to database
         */
        public OfficeDTO AddNewOffice(OfficeDTO officeDto)
        {
            Office domain =
                _officeFactory.Create(officeDto: officeDto);

#if DEBUG
            Trace.WriteLine(message: "Domain model:" + domain.ToString());
#endif
            if (!ValidateDomainModel(u: domain))
            {
                return null;
            }

            var ret = _officeRepository.Add(entity: domain);
            _officeRepository.SaveChanges();

            return _officeFactory.Create(office: ret);
        }
    }
}
    

