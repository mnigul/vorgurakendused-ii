﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using BL.DTOs;
using BL.Interfaces;
using BL.ObjectFactories;
using Domain;
using Interfaces.Repositories;

namespace BL.Services
{
        public class CompanyService : Service, ICompanyService
        {
            private readonly ICompanyRepository _companyRepository;
            private readonly CompanyFactory _companyFactory;

            public CompanyService(ICompanyRepository companyRepository)
            {
                _companyRepository = companyRepository;

                _companyFactory = new CompanyFactory();
            }

            /**
             * Get Company as DTO from database by ID
             */
            public CompanyDTO GetById(int id)
            {
                var company = _companyRepository.Find(id: id);
                return _companyFactory.Create(company: company);
            }

            /**
             * Get all Company-s as DTOs from database
             */
            public List<CompanyDTO> GetAll()
            {
                return _companyRepository
                    .All
                    .Select(selector: x => _companyFactory.Create(company: x))
                    .ToList();
            }

            /**
             *  Add a new Comapny to database
             */
            public CompanyDTO AddNewCompany(CompanyDTO companyDto)
            {
                //create Comapny model
                Company domain = _companyFactory.Create(companyDto: companyDto);
                //add missing Company type by id

#if DEBUG
                Trace.Write(message: domain.ToString());
#endif

                if (!ValidateDomainModel(u: domain))
                {
                    return null;
                }

                var ret = _companyRepository.Add(entity: domain);
                _companyRepository.SaveChanges();

                return _companyFactory.Create(company: ret);
            }


        }
}
