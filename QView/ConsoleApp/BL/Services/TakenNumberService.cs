﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using BL.DTOs;
using BL.Interfaces;
using BL.ObjectFactories;
using Domain;
using Interfaces.Repositories;

namespace BL.Services
{
    public class TakenNumberService : Service, ITakenNumberService
    {
        private readonly ITakenNumberRepository _takenNumberRepository;
        private readonly TakenNumberFactory _takenNumberFactory;
        public TakenNumberService(ITakenNumberRepository takenNumberRepository)
        {
            _takenNumberRepository = takenNumberRepository;
            _takenNumberFactory = new TakenNumberFactory();
        }

        /**
         * Get TakenNumber as DTO from database by ID
         */
        public TakenNumberDTO GetById(int id)
        {
            var takenNumber = _takenNumberRepository.Find(id: id);
            return _takenNumberFactory.Create(takenNumber: takenNumber);
        }

        /**
         * Get all TakenNumber-s as DTOs from database
         */
        public List<TakenNumberDTO> GetAll()
        {
            return _takenNumberRepository
                .All
                .Select(selector: x => _takenNumberFactory.Create(takenNumber: x))
                .ToList();
        }

        /**
         *  Add a new TakenNumber to database
         */
        public TakenNumberDTO AddNewTakenNumber(TakenNumberDTO takenNumberDto)
        {
            TakenNumber domain = 
                _takenNumberFactory.Create(takenNumberDto: takenNumberDto);

#if DEBUG
            Trace.WriteLine(message:"Domain model:" + domain.ToString());
#endif
            if (!ValidateDomainModel(u: domain))
            {
                return null;
            }
            //validate dateTime for sql
            domain.DateTaken = ValidateDateTimeForDateTimeNow(d: domain.DateTaken);
            //domain.DateService = ValidateDateTimeForSqlDateTimeMin(d: domain.DateService ?? DateTime.MinValue);


            var ret = _takenNumberRepository.Add(entity: domain);
            _takenNumberRepository.SaveChanges();

            return _takenNumberFactory.Create(takenNumber: ret);
        }
    }
}
