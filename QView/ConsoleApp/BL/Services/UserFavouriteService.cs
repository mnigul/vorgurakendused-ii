﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using BL.DTOs;
using BL.Interfaces;
using BL.ObjectFactories;
using Domain;
using Interfaces.Repositories;

namespace BL.Services
{
    public class UserFavouriteService : Service, IUserFavouriteService
    {
        private readonly IUserFavouriteRepository _UserFavouriteRepository;
        private readonly UserFavouriteFactory _UserFavouriteFactory;

        public UserFavouriteService(IUserFavouriteRepository UserFavouriteRepository)
        {
            _UserFavouriteRepository = UserFavouriteRepository;
            _UserFavouriteFactory = new UserFavouriteFactory();
        }

        /**
         * Get UserFavourite as DTO from database by ID
         */
        public UserFavouriteDTO GetById(int id)
        {
            var userFavourite = _UserFavouriteRepository.Find(id: id);
            return _UserFavouriteFactory.Create(userFavourite: userFavourite);
        }

        /**
         * Get all UserFavourite-s as DTOs from database
         */
        public List<UserFavouriteDTO> GetAll()
        {
            return _UserFavouriteRepository
                .All
                .Select(selector: x => _UserFavouriteFactory.Create(userFavourite: x))
                .ToList();
        }

        /**
         *  Add a new UserFavourite to database
         */
        public UserFavouriteDTO AddNewUserFavourite(UserFavouriteDTO userFavouriteDto)
        {
            //create UserFavourite model
            UserFavourite domain = _UserFavouriteFactory.Create(userFavouriteDto: userFavouriteDto);
            //add missing user type by id

#if DEBUG
            Trace.Write(message: domain.ToString());
#endif

            if (!ValidateDomainModel(u: domain))
            {
                return null;
            }

            var ret = _UserFavouriteRepository.Add(entity: domain);
            _UserFavouriteRepository.SaveChanges();

           return _UserFavouriteFactory.Create(userFavourite: ret);
        }

    }
}