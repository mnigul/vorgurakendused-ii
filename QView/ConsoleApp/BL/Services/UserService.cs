﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using BL.DTOs;
using BL.Interfaces;
using BL.ObjectFactories;
using Domain;
using Interfaces.Repositories;

namespace BL.Services
{
    public class UserService : Service, IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly UserFactory _userFactory;
        //private readonly IUserTypeRepository _userTypeRepository;

        
        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
            
            _userFactory = new UserFactory();
        }

        /**
         * Get User as DTO from database by ID
         */
        public UserDTO GetById(int id)
        {
            var user = _userRepository.Find(id: id);
            return _userFactory.Create(user: user);
        }

        /**
         * Get all User-s as DTOs from database
         */
        public List<UserDTO> GetAll()
        {
            return _userRepository
                .All
                .Select(selector: x => _userFactory.Create(user: x))
                .ToList();
        }

        /**
         *  Add a new User to database
         */
        public UserDTO AddNewUser(UserDTO userDto)
        {
            //create User model
            User domain = _userFactory.Create(userDto: userDto);
            //add missing user type by id
            
#if DEBUG
            Trace.Write(message: domain.ToString());
#endif

            if (!ValidateDomainModel(u: domain))
            {
                return null;
            }

            var ret = _userRepository.Add(entity: domain);
            _userRepository.SaveChanges();

            return _userFactory.Create(user: ret);
        }

        
    }
}
