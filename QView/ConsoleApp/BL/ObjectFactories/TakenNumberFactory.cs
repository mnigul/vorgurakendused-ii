﻿using System;
using System.Data.SqlTypes;
using System.Diagnostics;
using BL.DTOs;
using Domain;

namespace BL.ObjectFactories
{
    public class TakenNumberFactory
    {
        public TakenNumberDTO Create(TakenNumber takenNumber)
        {
            return new TakenNumberDTO()
            {
                Id = takenNumber.Id,
                Number = takenNumber.Number,
                DateTaken = takenNumber.DateTaken,
                OfficeId = takenNumber.OfficeId,
                DateService = takenNumber.DateService,
                WaitTime = takenNumber.WaitTime
            };
        }

        public TakenNumber Create(TakenNumberDTO takenNumberDto)
        {

            return new TakenNumber()
            {
                //Id = takenNumberDto.Id,
                Number = takenNumberDto.Number,
                DateTaken = takenNumberDto.DateTaken,
                OfficeId = takenNumberDto.OfficeId,
                DateService = takenNumberDto.DateService,
                WaitTime = takenNumberDto.WaitTime
            };
        }
    }
}
