﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL.DTOs;
using Domain;

namespace BL.ObjectFactories
{
    public class RegionFactory
    {
        public RegionDTO Create(Region region)
        {
            return new RegionDTO()
            {
                Id = region.Id,
                Name = region.Name,
                Description = region.Description
            };
        }

        public Region Create(RegionDTO regionDto)
        {
            return new Region()
            {
                //Id = regionDto.Id,
                Name = regionDto.Name,
                Description = regionDto.Description
            };
        }

    }
}
