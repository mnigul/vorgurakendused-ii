﻿using BL.DTOs;
using Domain;

namespace BL.ObjectFactories
{
    public class OfficeFactory
    {
        public OfficeDTO Create(Office office)
        {
            return new OfficeDTO()
            {
                Id = office.Id,
                Name = office.Name,
                Address = office.Address,
                OpeningHours = office.OpeningHours,
                CityId = office.CityId,
                CompaniesId = office.CompanyId
            };
        }

        public Office Create(OfficeDTO officeDto)
        {
            return new Office()
            {
                Id = officeDto.Id,
                Name = officeDto.Name,
                Address = officeDto.Address,
                OpeningHours = officeDto.OpeningHours,
                CityId = officeDto.CityId,
                CompanyId = officeDto.CompaniesId
            };
        }
    }
}