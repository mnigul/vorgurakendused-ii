﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL.DTOs;
using Domain;

namespace BL.ObjectFactories
{
    public class CompanyFactory
    {
        public CompanyDTO Create(Company company)
        {
            return new CompanyDTO()
            {
                Id = company.Id,
                Name = company.Name,
                CompanyTypeId = company.CompanyTypeId,
                CompanyType = company.CompanyType,
                Offices = company.Offices

            };
        }

        public Company Create(CompanyDTO companyDto)
        {
            return new Company()
            {
                Id = companyDto.Id,
                Name = companyDto.Name,
                CompanyTypeId = companyDto.CompanyTypeId,
                CompanyType = companyDto.CompanyType,
                Offices = companyDto.Offices

            };
        }
    }
}
