﻿using System;
using BL.DTOs;
using Domain;

namespace BL.ObjectFactories
{
    public class UserFactory
    {
        public UserDTO Create(User user)
        {
            return new UserDTO()
            {
                Id = user.Id,
                UserName = user.UserName,
                FirstName = user.FirstName,
                LastName = user.LastName,
                PassWord = user.Password,
                MobileNumber = user.MobileNumber,
                Email = user.Email,
                DateCreated = user.DateCreated,
                UserTypeId = user.UserTypeId
            };
        }

        public User Create(UserDTO userDto)
        {
            return new User()
            {
                //Id = userDto.Id,
                UserName = userDto.UserName,
                FirstName = userDto.FirstName,
                LastName = userDto.LastName,
                Password = userDto.PassWord,
                MobileNumber = userDto.MobileNumber,
                Email = userDto.Email,
                DateCreated = DateTime.Now,
                UserTypeId = userDto.UserTypeId
                
            };
        }
    }
}
