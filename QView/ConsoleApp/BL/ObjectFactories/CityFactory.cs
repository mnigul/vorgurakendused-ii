﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL.DTOs;
using Domain;

namespace BL.ObjectFactories
{
    public class CityFactory
    {
        public CityDTO Create(City city)
        {
            return new CityDTO()
            {
                Id = city.Id,
                Name = city.Name,
                RegionId = city.RegionId
            };
        }

        public City Create(CityDTO cityDto)
        {
            return new City()
            {
                //Id = cityDto.Id,
                Name = cityDto.Name,
                RegionId = cityDto.RegionId
            };
        }
    }
}
