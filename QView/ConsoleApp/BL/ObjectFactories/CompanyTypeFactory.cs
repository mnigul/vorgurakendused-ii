﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL.DTOs;
using Domain;


namespace BL.ObjectFactories
{
    public class CompanyTypeFactory
    {
        public CompanyTypeDTO Create(CompanyType companyType)
        {
            return new CompanyTypeDTO()
            {
                Id = companyType.Id,
                Name = companyType.Name,
                Description = companyType.Description
            };
        }

        public CompanyType Create(CompanyTypeDTO companyTypeDto)
        {
            return new CompanyType(companyTypeDto.Name)
            {
                Id = companyTypeDto.Id,
                Name = companyTypeDto.Name,
                Description = companyTypeDto.Description

            };
        }
    }
}
