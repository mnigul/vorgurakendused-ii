﻿using BL.DTOs;
using Domain;

namespace BL.ObjectFactories
{
    public class UserFavouriteFactory
    {
        public UserFavouriteDTO Create(UserFavourite userFavourite)
        {
            return new UserFavouriteDTO()
            {
                Id = userFavourite.Id,
                DateCreated = userFavourite.DateCreated,
                DateEnded = userFavourite.DateEnded,
                OfficeId = userFavourite.OfficeId,
                UserId = userFavourite.UserId
            };
        }

        public UserFavourite Create(UserFavouriteDTO userFavouriteDto)
        {
            // Konstruktorisse poleks vaja UserID-d kaasa anda, kuna peab olema teada, mis kasutaja on sisse loginud!
            return new UserFavourite()
            {
                Id = userFavouriteDto.Id,
                DateCreated = userFavouriteDto.DateCreated,
                DateEnded = userFavouriteDto.DateEnded,
                OfficeId = userFavouriteDto.OfficeId,
                UserId = userFavouriteDto.UserId
            };
        }
    }
}