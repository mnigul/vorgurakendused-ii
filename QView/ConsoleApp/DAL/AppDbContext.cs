﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Helpers;
using Domain;
using Interfaces;

namespace DAL
{
    public class AppDbContext : DbContext, IAppDataContext  
    {
        public DbSet<City> Cities { get; set; }

        public DbSet<Company> Companies { get; set; }

        public DbSet<CompanyType> CompanyTypes { get; set; }

        public DbSet<Office> Offices { get; set; }

        public DbSet<PermissionApplied> PermissionApplieds { get; set; }

        public DbSet<Region> Regions { get; set; }

        public DbSet<TakenNumber> TakenNumbers { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<UserFavourite> UserFavourites { get; set; }

        public DbSet<UserPermission> UserPermissions { get; set; }

        public DbSet<UserType> UserTypes { get; set; }

        public AppDbContext() : base(nameOrConnectionString: "name = QViewConnectionString")
        {
            Database.SetInitializer(strategy: new DbInitializator());

#if DEBUG
            Database.Log = s => Trace.Write(message: s.Contains(value: "SELECT") ? s : "");
#else
            Database.Log = s => Console.WriteLine(s.Contains("SELECT") ? s : "");
#endif


        }

    }
}
