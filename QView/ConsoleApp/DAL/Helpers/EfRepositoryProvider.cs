﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interfaces;
using Interfaces.Repositories;

namespace DAL.Helpers
{
    public class EfRepositoryProvider : IRepositoryProvider
    {
        private readonly IAppDataContext _appDataContext;
        private readonly IRepositoryFactories _repositoryFactories;

        public EfRepositoryProvider(IAppDataContext appDataContext, IRepositoryFactories repositoryFactories)
        {
            _appDataContext = appDataContext;
            _repositoryFactories = repositoryFactories;
        }

        // this is the cache of already created repositories - dont create several instances of same repository
        protected Dictionary<Type, object> Repositories { get; } = new Dictionary<Type, object>();

        public IRepository<TEntity> GetStandardRepo<TEntity>() where TEntity : class
        {
            // get repo by type and provide factory for creating the repo when its not found
            return GetRepository<IRepository<TEntity>>(factory: _repositoryFactories.GetStandardRepositoryFactory<TEntity>());
        }

        public TRepoInterface GetCustomRepo<TRepoInterface>()
        {
            // get repo by type and provide factory for creating the repo when its not found
            return GetRepository<TRepoInterface>(factory: _repositoryFactories.GetCustomRepositoryFactory<TRepoInterface>());
        }

        private TRepository GetRepository<TRepository>(
            Func<IAppDataContext, object> factory)
        {
            object repositoryObject;

            // try to get repo from cache
            Repositories.TryGetValue(key: typeof(TRepository), value: out repositoryObject);

            // found it?
            if (repositoryObject != null)
            {
                return (TRepository)repositoryObject;
            }

            // if repo was not found in cache - create it
            return MakeRepository<TRepository>(factory: factory, appDataContext: _appDataContext);
        }

        private TRepository MakeRepository<TRepository>(Func<IAppDataContext, object> factory, IAppDataContext appDataContext)
        {
            if (factory == null)
            {
                throw new ArgumentNullException(paramName: $"No factory found for repo {typeof(TRepository).FullName}");
            }

            // create repo, use the supplied factory
            var repo = (TRepository)factory(arg: appDataContext);

            //save repo to dictionary
            Repositories[key: typeof(TRepository)] = repo;
            return repo;
        }
    }
}
