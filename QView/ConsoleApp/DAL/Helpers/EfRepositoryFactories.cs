﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Repositories;
using Interfaces;
using Interfaces.Repositories;

namespace DAL.Helpers
{
    public class EfRepositoryFactories : IRepositoryFactories
    {
        // list all your custom repos here - match interfaces with real classes
        // dictionary of func, key is the type
        
        private static readonly Dictionary<Type, Func<IAppDataContext, object>> CustomRepositoryFactories = new Dictionary<Type, Func<IAppDataContext, object>>()
                {
                    { typeof(IUserRepository),
                    appDataContext => new UserRepository(dbContext: appDataContext)},
                };
                
        
        // return func which takes one parameter and returns object
        public Func<IAppDataContext, object> GetStandardRepositoryFactory<TEntity>() where TEntity : class
        {
            // there is only one fuction to create the standard repo
            return appDataContext => new EfRepository<TEntity>(dbContext: appDataContext);
        }
        
        // return function which takes one parameter and returns object
        public Func<IAppDataContext, object> GetCustomRepositoryFactory<TRepoInterface>()
        {
            // try to get the func from dictionary
            Func<IAppDataContext, object> customRepositoryFactory;
            CustomRepositoryFactories.TryGetValue(key: typeof(TRepoInterface), value: out customRepositoryFactory);
            return customRepositoryFactory;
        }
        
    }
}
