﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using Domain.Enums;

namespace DAL.Helpers
{
    class DbInitializator : DropCreateDatabaseIfModelChanges<AppDbContext>
    {
        protected override void Seed(AppDbContext context)
        {
            /*
            //base.Seed(context: context);
            List<UserType> userTypes = GetInitialUserTypes();
            foreach (var initialUserType in userTypes)
            {
                context.UserTypes.Add(entity: initialUserType);
#if DEBUG
                Trace.WriteLine(message: "Added usertype " + initialUserType.Name);
#endif
            }
            List<User> users = GetInitialUsers();
            foreach (var user in users)
            {
                user.UserType = userTypes[index: 0];
                context.Users.Add(entity: user);

#if DEBUG
                Trace.WriteLine(message: "Added user " + user.UserName);
#endif
            }
            */

            List<CompanyType> companyTypes = GetInitialCompanyTypes();
            foreach (var companyType in companyTypes)
            {
                context.CompanyTypes.Add(entity: companyType);
#if DEBUG
                Trace.WriteLine(message: "Added companytype " + companyType.Name);
#endif
            }

            List<Company> companies = GetInitialCompanies(types: companyTypes);
            foreach (var company in companies)
            {
                context.Companies.Add(entity: company);
#if DEBUG
                Trace.WriteLine(message: "Added company " + company.Name);
#endif
            }

            List<Region> regions = GetInitialRegions();
            foreach (var region in regions)
            {
                context.Regions.Add(entity: region);
#if DEBUG
                Trace.WriteLine(message: "Added region " + region.Name);
#endif
            }

            List<City> cities = GetInitialCities(regions: regions);
            foreach (var city in cities)
            {
                context.Cities.Add(entity: city);
#if DEBUG
                Trace.WriteLine(message: "Added city " + city.Name);
#endif
            }

            List<Office> offices = GetInitialOffices(cities: cities, companies: companies);
            foreach (var office in offices)
            {
                context.Offices.Add(entity: office);
#if DEBUG
                Trace.WriteLine(message: "Added office " + office.Name);
#endif
            }
            /*
            List<UserFavourite> userFavourites = GetInitialUserFavourites(users: users, offices: offices);
            foreach (var userFavourite in userFavourites)
            {
                context.UserFavourites.Add(entity: userFavourite);
#if DEBUG
                Trace.WriteLine(message: "Added userFavourite " + userFavourite);
#endif
            }
            */
            context.SaveChanges();
        }


        private List<User> GetInitialUsers()
        {
            List<User> users = new List<User>();
            User person1 = new User()
            {
                UserName = "sassu",
                FirstName = "Lisandra",
                LastName = "Noor",
                Email = "sassu@gmail.com",
                MobileNumber = "123456",
                Password = "parool69",
                DateCreated = DateTime.Now
            };
            users.Add(item: person1);

            User person2 = new User()
            {
                UserName = "matz",
                FirstName = "Martin",
                LastName = "Nigul",
                Password = "parool69",
                Email = "matzu@gmail.com",
                MobileNumber = "123456",
                DateCreated = DateTime.Now
            };
            users.Add(item: person2);

            User person3 = new User()
            {
                UserName = "mmuru",
                FirstName = "Margus",
                LastName = "Muru",
                Password = "parooliga",
                Email = "margusmuru@gmail.com",
                MobileNumber = "123456",
                DateCreated = DateTime.Now,
                UserType = new UserType()
                {
                    Name = "AdminUser"
                }
            };
            users.Add(item: person3);

            return users;
        }

        private List<UserType> GetInitialUserTypes()
        {
            return new List<UserType>
            {
                new UserType(
                    name: "Standard User",
                    description: "Standard user without elevated privileges"
                ),
                new UserType(
                    name: "Admin User",
                    description: "Administrator with elevated permissions"
                ),
                new UserType(
                    name: "Provider User",
                    description: "Account type for number providers to add offices, locations, etc"
                )
            };
        }

        private List<CompanyType> GetInitialCompanyTypes()
        {
            return new List<CompanyType>
            {
                new CompanyType(
                    name: "Fun",
                    description: "Fun nights"
                )
            };
        }

        private List<Company> GetInitialCompanies(List<CompanyType> types)
        {
            return new List<Company>
            {
                new Company(name:"Nõukase ema", companyTypeId: types[index: 0].Id)
            };
        }

        private List<Region> GetInitialRegions()
        {
            return new List<Region>
            {
                new Region(name: "Harjumaa")
            };
        }

        private List<City> GetInitialCities(List<Region> regions)
        {
            return new List<City>
            {
                new City(name: "Tallinn", regionId: regions[index: 0].Id)
            };
        }

        // DOESN'T CREATE NEW USERFAVOURITE FOR SOME REASON!
        private List<UserFavourite> GetInitialUserFavourites(List<User> users, List<Office> offices)
        {
            return new List<UserFavourite>
            {
                new UserFavourite(officeId: offices[index: 0].Id, userId: users[index: 0].Id)
            };
        }

        private List<Office> GetInitialOffices(List<City> cities, List<Company> companies)
        {
            return new List<Office>
            {
                new Office(
                    name: "Peakontor",
                    aadress: "Kuskil Viimsi pool",
                    openingHours: "All night long",
                    cityId: cities[index: 0].Id,
                    companyId: companies[index: 0].Id
                    ),
                new Office(
                    name: "Swedbank Liivalaia",
                    aadress: "Liivalaia 85",
                    openingHours: "10-17",
                    cityId: cities[index: 0].Id,
                    companyId: companies[index: 0].Id
                ),
                new Office(
                    name: "Telia Kristiine",
                    aadress: "Kristiine 52",
                    openingHours: "10-17",
                    cityId: cities[index: 0].Id,
                    companyId: companies[index: 0].Id
                )
            };
        }
    }
}
