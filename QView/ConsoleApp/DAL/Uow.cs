﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using Interfaces;
using Interfaces.Repositories;

namespace DAL
{
    public class Uow : IUow
    {
        private readonly IAppDataContext _appDataContext;
        private readonly IRepositoryProvider _repositoryProvider;
        public Uow(IAppDataContext appDataContext, IRepositoryProvider repositoryProvider)
        {
            _appDataContext = appDataContext;
            _repositoryProvider = repositoryProvider;
        }

        // standard repos, based on IRepository/EFRepository. just update IUow and Uow
        public IRepository<User> Users => GetStandardRepo<User>();

        // list all your custom repos and their creation mechanism in dictionary - in Helpers/EfRepositoryFactories
        // add declarations in IUow also!
        //public IUserRepository Users => GetCustomRepo<IUserRepository>();

        public IUserRepository UsersRepository => GetCustomRepo<IUserRepository>();

        private IRepository<TEntity> GetStandardRepo<TEntity>() where TEntity : class
        {
            return _repositoryProvider.GetStandardRepo<TEntity>();
        }

        private TRepoInterface GetCustomRepo<TRepoInterface>()
        {
            return _repositoryProvider.GetCustomRepo<TRepoInterface>();
        }

        public int SaveChanges()
        {
            return ((DbContext)_appDataContext).SaveChanges();
        }

        
    }
}
