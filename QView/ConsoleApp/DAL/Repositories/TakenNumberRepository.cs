﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using Interfaces;
using Interfaces.Repositories;

namespace DAL.Repositories
{
    public class TakenNumberRepository : EfRepository<TakenNumber>, ITakenNumberRepository
    {
        public TakenNumberRepository(IAppDataContext dbContext) : base(dbContext: dbContext)
        {

        }
    }
}
