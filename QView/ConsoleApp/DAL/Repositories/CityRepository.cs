﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using Interfaces;
using Interfaces.Repositories;

namespace DAL.Repositories
{
    public class CityRepository : EfRepository<City>, ICityRepository
    {
        public CityRepository(IAppDataContext dbContext) : base(dbContext: dbContext)
        {

        }
    }
}
