﻿using System.Data.Entity;
using Domain;
using Interfaces;
using Interfaces.Repositories;

namespace DAL.Repositories
{
    public class UserRepository : EfRepository<User>, IUserRepository
    {
        public UserRepository(IAppDataContext dbContext) : base(dbContext: dbContext)
        {

        }

        public bool CheckExistingUserName(string userName)
        {
            throw new System.NotImplementedException();
        }
    }
}
