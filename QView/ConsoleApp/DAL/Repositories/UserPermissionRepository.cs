﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using Interfaces;
using Interfaces.Repositories;

namespace DAL.Repositories
{
    public class UserPermissionRepository : EfRepository<UserPermission>, IUserPermissionRepository
    {
        public UserPermissionRepository(IAppDataContext dbContext) : base(dbContext: dbContext)
        {
        }
    }
}
