﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using Interfaces;
using Interfaces.Repositories;

namespace DAL.Repositories
{
    public class RegionRepository : EfRepository<Region>, IRegionRepository
    {
        public RegionRepository(IAppDataContext dbContext) : base(dbContext: dbContext)
        {

        }
    }
}
