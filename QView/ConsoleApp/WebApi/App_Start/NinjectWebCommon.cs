using BL.Interfaces;
using BL.Services;
using Interfaces.Repositories;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(WebApi.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(WebApi.App_Start.NinjectWebCommon), "Stop")]

namespace WebApi.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using Interfaces;
    using DAL;
    using DAL.Helpers;
    using DAL.Repositories;

    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IAppDataContext>().To<AppDbContext>().InRequestScope();
            kernel.Bind<IRepositoryFactories>().To<EfRepositoryFactories>().InSingletonScope();
            kernel.Bind<IRepositoryProvider>().To<EfRepositoryProvider>().InRequestScope();
            kernel.Bind<IUow>().To<Uow>().InRequestScope();
            /*
            kernel.Bind<ISubjectService>().To<SubjectService>().InRequestScope();
            kernel.Bind<ISubjectRepository>().To<SubjectRepository>().InRequestScope();
            kernel.Bind<IPersonRepository>().To<PersonRepository>().InRequestScope();
            kernel.Bind<IPersonService>().To<PersonService>().InRequestScope();
            */

            kernel.Bind<IUserRepository>().To<UserRepository>().InRequestScope();
            kernel.Bind<IUserService>().To<UserService>().InRequestScope();

            kernel.Bind<ITakenNumberRepository>().To<TakenNumberRepository>().InRequestScope();
            kernel.Bind<ITakenNumberService>().To<TakenNumberService>().InRequestScope();

            kernel.Bind<IOfficeRepository>().To<OfficeRepository>().InRequestScope();
            kernel.Bind<IOfficeService>().To<OfficeService>().InRequestScope();

            kernel.Bind<IUserFavouriteRepository>().To<UserFavouriteRepository>().InRequestScope();
            kernel.Bind<IUserFavouriteService>().To<UserFavouriteService>().InRequestScope();

            kernel.Bind<IRegionRepository>().To<RegionRepository>().InRequestScope();
            kernel.Bind<IRegionService>().To<RegionService>().InRequestScope();

            kernel.Bind<ICompanyTypeRepository>().To<CompanyTypeRepository>().InRequestScope();
            kernel.Bind<ICompanyTypeService>().To<CompanyTypeService>().InRequestScope();

            kernel.Bind<ICompanyRepository>().To<CompanyRepository>().InRequestScope();
            kernel.Bind<ICompanyService>().To<CompanyService>().InRequestScope();

            kernel.Bind<ICityRepository>().To<CityRepository>().InRequestScope();
            kernel.Bind<ICityService>().To<CityService>().InRequestScope();
        }
    }
}