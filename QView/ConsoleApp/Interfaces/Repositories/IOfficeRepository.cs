﻿using Domain;

namespace Interfaces.Repositories
{
    public interface IOfficeRepository : IRepository<Office>
    {
        
    }
}
