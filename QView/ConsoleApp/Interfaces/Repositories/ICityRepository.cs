﻿using Domain;

namespace Interfaces.Repositories
{
    public interface ICityRepository : IRepository<City>
    {

    }
}
