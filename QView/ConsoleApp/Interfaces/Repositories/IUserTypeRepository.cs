﻿using Domain;

namespace Interfaces.Repositories
{
    public interface IUserTypeRepository : IRepository<UserType>
    {
    }
}
