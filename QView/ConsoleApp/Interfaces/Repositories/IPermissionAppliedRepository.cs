﻿using Domain;

namespace Interfaces.Repositories
{
    public interface IPermissionAppliedRepository : IRepository<PermissionApplied>
    {
    }
}
