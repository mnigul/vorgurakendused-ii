﻿using Domain;

namespace Interfaces.Repositories
{
    public interface ITakenNumberRepository : IRepository<TakenNumber>
    {
    }
}
