﻿using Domain;

namespace Interfaces.Repositories
{
    public interface IUserRepository : IRepository<User>
    {
        bool CheckExistingUserName(string userName);
    }
}
