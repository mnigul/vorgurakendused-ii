﻿using Domain;

namespace Interfaces.Repositories
{
    public interface ICompanyTypeRepository : IRepository<CompanyType>
    {
    }
}
