﻿using Domain;

namespace Interfaces.Repositories
{
    public interface IRegionRepository : IRepository<Region>
    {
    }
}
