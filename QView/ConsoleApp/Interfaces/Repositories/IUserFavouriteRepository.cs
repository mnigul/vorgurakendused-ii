﻿using Domain;

namespace Interfaces.Repositories
{
    public interface IUserFavouriteRepository : IRepository<UserFavourite>
    {
    }
}
