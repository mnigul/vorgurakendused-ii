﻿using Domain;

namespace Interfaces.Repositories
{
    public interface IUserPermissionRepository : IRepository<UserPermission>
    {
    }
}
