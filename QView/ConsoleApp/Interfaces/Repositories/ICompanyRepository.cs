﻿using Domain;

namespace Interfaces.Repositories
{
    public interface ICompanyRepository : IRepository<Company>
    {
    }
}
