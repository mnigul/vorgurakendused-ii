﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using Interfaces.Repositories;

namespace Interfaces
{
    public interface IUow
    {
        IRepository<User> Users { get; }

        IUserRepository UsersRepository { get; }

        int SaveChanges();
    }
}
